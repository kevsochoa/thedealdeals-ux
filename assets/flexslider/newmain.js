$(document).ready(function(){
	
	//waypoints animation
	$('#effect_1').waypoint(function() {
	   $(this).addClass('animated tada');
	   $(this).one('webkitAnimationEnd oanimationend msAnimationEnd animationend',   
		    function(e) {
		    $('#panel_1_1 .speech-bubble').addClass('animated bounceIn');
		   
			   setTimeout(function(){
			   		$('#panel_1_2 .speech-bubble').addClass('animated fadeInLeft');
			   }, 1000);

			   setTimeout(function(){
			   		$('#panel_2 #call_to_action').addClass('animated rollIn');
			   }, 2000);
		   
		});



	}, {
	    offset: '75%'
	});


	 $('#panel_3_1 .speech-bubble').waypoint(function() {
	    $(this).addClass('animated fadeInLeft');
	       $(this).one('webkitAnimationEnd oanimationend msAnimationEnd animationend',   
	    	    function(e) {
	    	    $('#panel_3_2 .speech-bubble').addClass('animated fadeInRight');
	    	   
	    		   setTimeout(function(){
	    		   		$('#bottom_links a').addClass('animated swing');
	    		   }, 3000);

	    	
	    	   
	    	});

	}, {
	    offset: '50%'
	});



	// $('#panel_1_1 .speech-bubble').waypoint(function() {
	//     $(this).addClass('animated bounceIn');
	// }, {
	//     offset: '100%'
	// });

	// $('#panel_1_2 .speech-bubble').waypoint(function() {
	//     $(this).addClass('animated fadeInLeft');
	// }, {
	//     offset: '75%'
	// });
	// $('#panel_2 #call_to_action').waypoint(function() {
	//     $(this).addClass('animated rollIn');
	// }, {
	//     offset: '100%'
	// });
	// $('#panel_3_1 .speech-bubble').waypoint(function() {
	//     $(this).addClass('animated fadeInLeft');
	// }, {
	//     offset: '75%'
	// });

	// $('#panel_3_2 .speech-bubble').waypoint(function() {
	//     $(this).addClass('animated fadeInRight');
	// }, {
	//     offset: '75%'
	// });

	// $('#bottom_links a').waypoint(function() {
	//     $(this).addClass('animated swing');
	// }, {
	//     offset: '75%'
	// });



	//hide the subtle gradient layer (.cd-pricing-list > li::after) when pricing table has been scrolled to the end (mobile version only)
	checkScrolling($('.cd-pricing-body'));
	$(window).on('resize', function(){
		window.requestAnimationFrame(function(){checkScrolling($('.cd-pricing-body'))});
	});
	$('.cd-pricing-body').on('scroll', function(){ 
		var selected = $(this);
		window.requestAnimationFrame(function(){checkScrolling(selected)});
	});

	function checkScrolling(tables){
		tables.each(function(){
			var table= $(this),
				totalTableWidth = parseInt(table.children('.cd-pricing-features').width()),
		 		tableViewport = parseInt(table.width());
			if( table.scrollLeft() >= totalTableWidth - tableViewport -1 ) {
				table.parent('li').addClass('is-ended');
			} else {
				table.parent('li').removeClass('is-ended');
			}
		});
	}




	//switch from monthly to annual pricing tables
	bouncy_filter($('.cd-pricing-container'));

	function bouncy_filter(container) {
		container.each(function(){
			var pricing_table = $(this);
			var filter_list_container = pricing_table.children('.cd-pricing-switcher'),
				filter_radios = filter_list_container.find('input[type="radio"]'),
				pricing_table_wrapper = pricing_table.find('.cd-pricing-wrapper');

			//store pricing table items
			var table_elements = {};
			filter_radios.each(function(){
				var filter_type = $(this).val();
				table_elements[filter_type] = pricing_table_wrapper.find('li[data-type="'+filter_type+'"]');
			});

			//detect input change event
			filter_radios.on('change', function(event){
				event.preventDefault();
				//detect which radio input item was checked
				var selected_filter = $(event.target).val();

				//give higher z-index to the pricing table items selected by the radio input
				show_selected_items(table_elements[selected_filter]);

				//rotate each cd-pricing-wrapper 
				//at the end of the animation hide the not-selected pricing tables and rotate back the .cd-pricing-wrapper
				
				if( !Modernizr.cssanimations ) {
					hide_not_selected_items(table_elements, selected_filter);
					pricing_table_wrapper.removeClass('is-switched');
				} else {
					pricing_table_wrapper.addClass('is-switched').eq(0).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {		
						hide_not_selected_items(table_elements, selected_filter);
						pricing_table_wrapper.removeClass('is-switched');
						//change rotation direction if .cd-pricing-list has the .cd-bounce-invert class
						if(pricing_table.find('.cd-pricing-list').hasClass('cd-bounce-invert')) pricing_table_wrapper.toggleClass('reverse-animation');
					});
				}
			});
		});
	}
	function show_selected_items(selected_elements) {
		selected_elements.addClass('is-selected');
	}

	function hide_not_selected_items(table_containers, filter) {
		$.each(table_containers, function(key, value){
	  		if ( key != filter ) {	
				$(this).removeClass('is-visible is-selected').addClass('is-hidden');

			} else {
				$(this).addClass('is-visible').removeClass('is-hidden is-selected');
			}
		});
	}

	// get the size of viewport
	function updateViewportDimensions() {
		var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
		return { width:x,height:y };
	}
	// setting the viewport width
	var viewport = updateViewportDimensions();

	$('.channel-wrapper').on('click', function(){
	  $(this).parent().toggleClass('active');
	  $(this).parent().siblings().toggleClass('inactive');
	})

	$('.mobile-trigger').on('change',function(){
		$('.mobile-trigger').not(this).prop('checked', false);
	});

	$('[data-toggle="tooltip"]').tooltip({
	    'placement': 'top'
	    });
	$('.setup-popover').popover();	

    $('.popup-close').click(function()
    {
    	$('.registration-form').addClass('hide');
    	$('#step3').addClass('hide');
    	$('.plan-calculator').removeClass('hide');
	  	$('#agree_terms').removeAttr('checked');
	    $('.popup-form-control').val('');
	    $('.popup-form-control').removeClass('error-border');
	    // $('.popup-bg').fadeOut();
	    $('.popup-bg').removeClass('show');
	    $('body , html').removeClass('no-overflow');
	});

	$('.btn-signup').click(function()
    {
		var plan = $(this).data('plan');
    	var subscription = $(this).data('subscription');

    	$('#type-' + plan).prop("checked", true);    	
		var planTitle = $('#type-' + plan).data('plan-title');
		var subscriptionTitle = $('#period-' + subscription).data('subscription-title');
    	$('#period-' + subscription).prop("checked", true);
    	computePlan();
    	withCustomURL();

    	if(plan == 'free' || plan == 'Free')
        {
	    	$('#step1').addClass('hide');
			$('#step2').removeClass('hide');
			$('.popupbox .registration-form .popup-header').html(planTitle);
		}

    	if(plan != 'free' && plan != 'Free')
        {
        	$('.popupbox .registration-form .popup-header').html(planTitle + ' - ' + subscriptionTitle);
        }

        $('.popup-bg').addClass('show');
        $('body , html').addClass('no-overflow');

    });


	$('.plan-calculator input[type=radio]').on('change',function(){
	    computePlan();    
    	withCustomURL();
	});
});

function computePlan()
{
	var subscription_id = 1;
	var subscription = $('.period-type:checked').val();
	var plan = $('.plan-type:checked').val();
	var planTitle = $('.plan-type:checked').data('plan-title');
	var subscriptionTitle = $('.period-type:checked').data('subscription-title');

	// This code is to set the subscription id that would be save on the database
	switch(subscription) {
	    case 'monthly':
			switch(plan) {
			    case 'free':
			        subscription_id = 1;
			        break;
			    case 'basic':
			        subscription_id = 2;	
			        break;
			    case 'premium':
			        subscription_id = 4;	
			        break;
			}
        break;
	    case 'yearly':
	    	switch(plan) {
			    case 'basic':
			        subscription_id = 3;	
		        break;
			    case 'premium':
			        subscription_id = 5;	
		        break;
			}
        break;
	    default:subscription_id = 1;
	}
	$('#service_plan_subscription_id').val(subscription_id);
	// This code is to set the subscription id that would be save on the database END
	var planPrice = $('.plan-type:checked').data(subscription + '-fee');
	var impPrice = $('.implementation-type:checked').data('implementation-fee');
	var planTotal = parseFloat(planPrice) + parseFloat(impPrice);
	$('.price-total').find('.total-amount').html(Number(planTotal).toLocaleString('en') );
	
	var popupHeader = planTitle;
	if(plan != 'free')
	{
		popupHeader = planTitle + ' - ' + subscriptionTitle;
	}
	$('.popupbox .registration-form .popup-header').html(popupHeader);
	$('#step3 .popup-header').html(popupHeader);
	$('.plan-type input[type=radio]').each(function()
	{
		var planPrice = $(this).data(subscription + '-fee');

		$(this).siblings('label').find('span i').text(Number(planPrice).toLocaleString('en') );
	});
}

function withCustomURL()
{
	var plan = $('.plan-type:checked').val();
	if(plan == 'basic' || plan == 'premium')
	{
		$('#custom_url_holder').show();
        $('#preview_custom_url_holder').show();
	}
	else
	{
		$('#custom_url').val('');
		$('#preview_custom_url').val('');
		$('#custom_url_holder').hide();
        $('#preview_custom_url_holder').hide();
	}
}

$(document).keyup(function(e)
{
  if(e.keyCode == 13) {}     // enter
  if(e.keyCode == 27)   // esc
  {
    $('#agree_terms').removeAttr('checked');
    $('.popup-form-control').val('');
    $('.popup-form-control').removeClass('error-border');
    $('.popup-bg').fadeOut(); 
  }
});

function copyCompanyEmail()
{
    var companyEmail = $('#company_email').val();
    $('#user_email').val(companyEmail);
    $('#user_confirm_email').val(companyEmail);
}
    
function prevStep(currentStep, prevStep)
{
    $('#' + prevStep).removeClass('hide');
    $('#' + currentStep).addClass('hide');
}

function checkRequired(id)
{
	if($('#' + id).val() == '')
	{
		return false;
	}
	else
	{
		$('#' + id).removeClass('error-border');
	}

	return true;
}
    
function isValidEmailAddress(emailAddress)
{
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}