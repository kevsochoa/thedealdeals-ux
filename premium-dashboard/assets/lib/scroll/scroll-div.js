<script type="text/javascript">
$(function() {
    // Set this variable with the height of your sidebar + header
    var offsetPixels = 700; 

    $(window).scroll(function() {
        if ($(window).scrollTop() > offsetPixels) {
            $( ".scrollingBox" ).css({
                "position": "fixed",
                "top": "55px"
            });
        } else {
            $( ".scrollingBox" ).css({
                "position": "static"
            });
        }
    });
});
</script>